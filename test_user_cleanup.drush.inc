<?php

/**
 * @file
 * Drush command hooks.
 */

/**
 * Implements hook_drush_command().
 */
function test_user_cleanup_drush_command() {
  $items = array();

  $items['test-user-cleanup'] = array(
    'description' => dt('Delete accounts tagged with the target role'),
    'examples' => array(
      'drush test-user-cleanup' => dt('Delete testing users.'),
    ),
  );

  return $items;
}

function drush_test_user_cleanup() {
  if (!drush_confirm(dt('Are you sure you want to continue?'))) {
    return;
  }

  if (!_test_user_cleanup_delete_users()) {
    drush_set_error('Test role has not been set');
  }
}
